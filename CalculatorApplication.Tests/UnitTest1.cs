using System;
using CalculatorApplication.Controllers;
using Xunit;

namespace CalculatorApplication.Tests
{
    public class UnitTest1
    {

        [Fact]
        public void TestAdd()
        {
            CalculatorController _calculator = new CalculatorController();
            
            Assert.Equal(-1, _calculator.Add(2, -3));
            Assert.Equal(5, _calculator.Add(2, 3));
        }

        [Fact]
        public void TestSubtract()
        {
            CalculatorController _calculator = new CalculatorController();
            Assert.Equal(2, _calculator.Subtract(5, 3));
        }

        [Fact]
        public void TestMultiply()
        {
            CalculatorController _calculator = new CalculatorController();
            Assert.Equal(15, _calculator.Multiply(3, 5));
        }

        [Fact]
        public void TestDivide()
        {
            CalculatorController _calculator = new CalculatorController();
            Assert.Equal(2, _calculator.Divide(6, 3));
        }

        [Fact]
        public void TestDivideByZero()
        {
            CalculatorController _calculator = new CalculatorController();
            Assert.Throws<DivideByZeroException>(() => _calculator.Divide(6, 0));
        }
    }
}